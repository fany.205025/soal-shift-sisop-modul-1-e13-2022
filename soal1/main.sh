#!/bin/bash

calendar=$(date +%D)
time=$(date +%T)
Log=/home/mahes/Seasoup/log/log.txt
User=/home/mahes/Seasoup/users/user.txt

func_login(){
	if [[ -n "$(grep $uname "$User")" ]] && [[ -n "$(grep $pword "$User")" ]]
		then
			echo "$calendar $time LOGIN: INFO User $uname logged in" >> $Log
			echo "You are logged in"

			printf "Mau ngaps gan? [dl // att]: "
			read command
			if [[ $command == att ]]
			then
				func_att
			elif [[ $command == dl ]]
			then
				func_dl
			else
				echo "Not found"
			fi

		elif [[ -n "$(grep $uname "$User")" ]]
			then
			echo "Wrong password"
			echo "$calendar $time LOGIN: ERROR Failed login attempt on user $uname " >> $Log
		else
			echo "User ini belum terdaftar"	
		fi
}

func_dl(){
	printf "Mau berapa kali? : "
	read n

	if [[ ! -f "$folder.zip" ]]
	then
		mkdir $folder
		func_start
	else
		func_unzip
	fi

}

func_unzip(){
	unzip -P $pword $folder.zip
	rm $folder.zip

}

func_start(){
	for(( i=1; i<=n; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
	done

	zip --password $pword -r $folder.zip $folder/
	rm -rf $folder
}

func_att(){
	if [[ ! -f "$Log" ]]
	then
		echo "Belum ada log"
	else
		awk -v user="$uname" 'BEGIN {logged=0} $6 == user {logged++} 
		END {print (logged), "Accepted login attemps are detected"}' $Log

		awk -v user="$uname" 'BEGIN {failed=0} $10 == user {failed++} 
		END {print (failed), "Failed login attemps are detected"}' $Log
	fi
}

echo "Hai login dlu ya!!"
printf "Username: "
read uname

echo "Password: "
read -s pword

folder=$(date +%Y-%m-%d)_$uname

func_login